package models;

import javax.persistence.Entity;

import play.db.ebean.Model;
import javax.persistence.Id;

@Entity
public class Account extends Model {
	public String username;
	public String password;
	public String role;
	public String fullname;
	public String diploma;
	public String studyGroup;
	
	public Account(){}
	
	public Account(String fullnm, String usrnm, String passwrd, String ro, String dipl, String gro){	
		this.username = usrnm;
		this.password = passwrd;
		this.role = ro;
		this.fullname = fullnm;
		this.diploma = dipl;
		this.studyGroup = gro;
	}
	
	public static Finder<Integer, Account> find
    = new Model.Finder<>(Integer.class, Account.class);
	
}
