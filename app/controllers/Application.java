package controllers;

import java.util.Arrays;
import java.util.List;

import org.apache.http.protocol.RequestDate;

import models.Account;

import play.*;
import play.api.mvc.Session;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.*;
import scala.annotation.meta.param;
import scala.collection.generic.BitOperations.Int;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return redirect("login");
    }

    public static Result login(){
    	return ok(login.render("Login please"));
    }

    public static Result checkInfo(){
    	List<Account> accounts = Account.find.all();
    	DynamicForm request = Form.form().bindFromRequest();
    	String username = request.get("username");
    	String password = request.get("password");
    	
    	for(Account acc : accounts){
    		if(acc.username.equals(username) && acc.password.equalsIgnoreCase(password)){
    			if(acc.role.equalsIgnoreCase("master")){
    				session("username", username);
    				return redirect("master");
    			}
    			else if(acc.role.equalsIgnoreCase("teacher")){
    				session("username", username);
    				return redirect("teacher");
    			}
    			else{
    				session("username", username);
    				return redirect("student");
    			}
    		}
    	}
    	
    	return ok(login.render("Sorry. Try again"));
    }
    
    public static Result master(){
    	String username = session().get("username");
    	List<Account> teachers = Account.find.all();
    	for(Account tea : teachers){
    		if(tea.username.equals(username)){
    			return ok(master.render(tea, teachers));
    		}
    	}
    	
    	return redirect("login");
    }
    
    public static Result save(){
    	if(session().isEmpty()) return redirect("login");
    	
    	DynamicForm request = Form.form().bindFromRequest();
    	String fullname = request.get("fullname");
    	String username = request.get("username");
    	String password = request.get("password");
    	String re_password = request.get("re_password");
    	String role = request.get("role");
    	String diploma = request.get("diploma");
    	String group = request.get("group");
    	
    	List<Account> accounts = Account.find.all();
    	if(!password.equals(re_password)){
    		return ok("Sorry");
    	}
    	
    	for(Account account : accounts){
    		if(account.username.equalsIgnoreCase(username)){
    			return ok("Sorry");
    		}
    	}
    	
    	Account acc = new Account(fullname, username, password, role, diploma, group);
    	acc.save();
    	
    	return redirect("master");
    }
    
    public static Result teacher(){
    	String username = session().get("username");
    	List<Account> teachers = Account.find.all();
    	for(Account tea : teachers){
    		if(tea.username.equals(username)){
    			return ok(teacher.render(tea));
    		}
    		
    	}
    	
    	return redirect("login");
    }
    
    public static Result edit(){
    	String username = session().get("username");
    	List<Account> teachers = Account.find.all();
    	for(Account tea : teachers){
    		if(tea.username.equals(username)){
    			return ok(edit.render(tea));
    		}
    		
    	}
    	
    	return redirect("login");
    }
    
    public static Result editSave(){
    	DynamicForm request = Form.form().bindFromRequest();
    	String fullname = request.get("fullname");
    	String diploma = request.get("diploma");
    	String group = request.get("group");
    	
    	String username = session().get("username");
    	List<Account> users = Account.find.all();
    	for(Account user : users){
    		if(user.username.equals(username)){
    			user.fullname = fullname;
    			user.diploma = diploma;
    			user.studyGroup = group;
    			user.save();
    			if(user.role.equals("teacher")) return redirect("teacher");
    			else return redirect("student");
    		}
    		
    	}
    	
    	return redirect("login");
    }
    
    public static Result editPass(){
    	return ok(edit_pass.render());
    }
    
    public static Result passSave(){
    	DynamicForm request = Form.form().bindFromRequest();
    	String old_pass = request.get("old_password");
    	String new_pass = request.get("new_password");
    	String re_pass = request.get("re_password");
    	
    	if(!new_pass.equals(re_pass)) return badRequest("Opps");
    	
    	String username = session().get("username");
    	List<Account> users = Account.find.all();
    	for(Account user : users){
    		if(user.username.equals(username)){
    			if(!user.password.equals(old_pass)) return badRequest("Opps");
    		
    			user.password = new_pass;
    			user.save();
    			if(user.role.equals("teacher")) return redirect("teacher");
    			else return redirect("student");
    		}
    		
    	}
    	
    	return redirect("login");
    }
    
    public static Result student(){
    	String username = session().get("username");
    	List<Account> teachers = Account.find.all();
    	for(Account tea : teachers){
    		if(tea.username.equals(username)){
    			return ok(student.render(tea));
    		}
    		
    	}
    	
    	return redirect("login");
    }
    
    
    public static Result logout(){
    	session().clear();
    	return redirect("login");
    }
    
}
