# --- Sample dataset

# --- !Ups

insert into account (username, password, role, fullname, diploma, study_group) values ('admin', 'admin', 'master', 'Nguyen Vo Lan Trinh', '', '');
insert into account (username, password, role, fullname, diploma, study_group) values ('hieptq', '123456', 'teacher', 'Ta Quang Hiep', 'Professional', '');
insert into account (username, password, role, fullname, diploma, study_group) values ('hieptq01', '123456', 'student', 'Nguyen Duc Trung', '', 'Computer Science');

# --- !Downs

delete from account;
