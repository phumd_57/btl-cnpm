# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table account (
  username                  varchar(255),
  password                  varchar(255),
  role                      varchar(255),
  fullname                  varchar(255),
  diploma                   varchar(255),
  study_group               varchar(255))
;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists account;

SET REFERENTIAL_INTEGRITY TRUE;

